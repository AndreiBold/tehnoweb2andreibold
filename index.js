const FIRST_NAME = "Andrei";
const LAST_NAME = "Bold";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function numberParser(value) {
  if (
    typeof value === "number" &&
    Number.MAX_VALUE > value &&
    Number.MIN_VALUE < value
  ) {
    return Math.floor(value);
  }

  if (typeof value === "string" && !isNaN(parseInt(value))) {
    return parseInt(value);
  }

  if (!(Number.MAX_VALUE < value && Number.MIN_VALUE > value)) return NaN;

  if (!isFinite(value)) {
    return NaN;
  }
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  numberParser
};
